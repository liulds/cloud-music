import React from 'react'
import './musiclist.scss'
import axios from 'axios'
import {LeftOutlined, HomeFilled,BranchesOutlined, PlayCircleOutlined} from '@ant-design/icons'
// import PubSub from 'pubsub-js'
import { useEffect } from 'react'
export default function Musiclist(props) {
  useEffect(() => {
    // PubSub.subscribe('getSonIdx',getSonIdx)
    // eslint-disable-next-line
  },[])
    // 处理多个作者合并
    let getauthor = (data) => {
        let author = []
        data.forEach(item => {
            author.push(item.name)
        })
        return author.join('/')
    }

    let getDesc = (data) => {
      if (data.length < 27){
        return data
      }else{
        return data.slice(0,27) + '...'
      }
    }

    let getmusicdetail = async (id, index) => {
      let {data:{songs:[songdet]}} = await axios('http://localhost:3000/song/detail?ids='+id)
      let data = []
      props.location.state.tracks.forEach(item=> data.push(item.id))
      window.sessionStorage.setItem('idx', index)
      window.sessionStorage.setItem('musiclist', JSON.stringify(data))
      window.sessionStorage.setItem('musicstatic', JSON.stringify(songdet))
      props.history.push({pathname:"/musicdetail"})
    }


  return (
    <div className='musiclist'>
      <div className='locationBtn'>
        <div><LeftOutlined style={{color:"#fff", fontSize:"0.2rem"}} onClick={()=>{props.history.push('/')}}/></div>
        <div className='fenge'></div>
        <div><HomeFilled style={{color:"#fff", fontSize:"0.2rem"}} onClick={()=>{props.history.push('/')}}/></div>
      </div>
      <div className='listheader' style={{ backgroundImage: props.location.state.headColor }}>歌单</div>
      <div className='listtop' style={{ backgroundImage: props.location.state.bodyColor  }}>
        <div className='top'>
          <div className='img'><img src={props.location.state.coverImgUrl} alt="" /></div>
          <div className='right'>
            <div className='header'>{props.location.state.name}</div>
            <div className='logo'>
              <div className='img'><img src={props.location.state.creator.avatarUrl} alt="" /></div>
              <div className='logoname'>{props.location.state.creator.nickname}</div>
            </div>
            <div className='desc'>{getDesc(props.location.state.description)}</div>
          </div>
        </div>
        <div className='fenxiang'><BranchesOutlined />分享给微信好友</div>
        <div className='musiclists'>
          <div className='baiotou' onClick={()=>{getmusicdetail(props.location.state.tracks[0].id, 0)}}>
            <PlayCircleOutlined />
            <span className='one'>播放全部</span>
            <b>(共{props.location.state.tracks.length}首)</b>
          </div>
          {
            props.location.state.tracks.map((item, index) => {
              return(
                <div key={index} className="musicitem" onClick={()=>getmusicdetail(item.id, index)}>
                  <div className='name'>
                    <div className='index'>{index+1<10 && (<span>&nbsp;&nbsp;</span>)}{index+1}</div>
                    <div className='title'>
                      <div className='mictitle'>{item.name}</div>
                      <div className='micauthor'>{getauthor(item.ar)}-{item.al.name}</div>
                    </div>
                  </div>
                  <div className='bofang'>
                    <PlayCircleOutlined style={{fontSize:"0.25rem",color:"#999999",marginTop:"0.1rem"}}/>
                  </div>
                </div>
              )
            })
          }
        </div>
      </div>
    </div>
  )
}
