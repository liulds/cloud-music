import React from 'react'
import './index.scss'
import axios from 'axios'
import { useEffect } from 'react'
import { useState } from 'react'
export default function Index(props) {
    let [list, setlist] = useState([])
    let [backColor] = useState([
        {
            headColor:"linear-gradient(to bottom,#D0A4B5, #C9829E,#C26F92,#C26C91, #C06A90)",
            bodyColor:"linear-gradient(to bottom,#C06A90, #B15D83)"
        },
        {
            headColor:"linear-gradient(to bottom,#9DC5BF, #7FBAB1,#66B1A6,#61AEA3, #60AEA3)",
            bodyColor:"linear-gradient(to bottom,#60AEA3, #3EA09A)"
        },
        {
            headColor:"linear-gradient(to bottom,#D2A8A0, #CD8A7F,#CB7B6E,#CA7568, #C97567)",
            bodyColor:"linear-gradient(to bottom,#C97567, #C3514D)"
        },
        {
            headColor:"linear-gradient(to bottom,#9DBFD0, #76ACC8,#64A2C5,#62A1C4, #61A0C4)",
            bodyColor:"linear-gradient(to bottom,#61A0C4, #5387B7)"
        }
    ])
    useEffect(() => {
        getlist()
    },[])
    
    let getauthor = (data) => {
        let author = []
        data.forEach(item => {
            author.push(item.name)
        })
        return author.join('/')
    }
    
    
    
    const getlist = async () => {
        let {data:{playlist: biaosheng}} = await axios.get('http://localhost:3000/top/list?idx=3')// 3-飙升榜
        let {data:{playlist:xinge}} = await axios.get('http://localhost:3000/top/list?idx=0')// 0-新歌榜
        let {data:{playlist:rege}} = await axios.get('http://localhost:3000/top/list?idx=1')// 1-热歌榜
        let {data:{playlist:yuanchuang}} = await axios.get('http://localhost:3000/top/list?idx=2')// 2-原创榜
        setlist([biaosheng, xinge, rege, yuanchuang])
    }
  return (
    <div className='index'>
        <div className='title'>网易云音乐</div>
        <div className='schinputdiv'>
        <input 
        type="text" 
        className='schinputdom'
        placeholder="搜索歌曲" 
        onClick={()=>props.history.push({pathname:"/searchmusic", state:{searchkey:''}})}
        />
        </div>
        <div className='typelist'>
            {
                list.map((item, index) => {
                    return(
                        <div key={index} className="tplist" onClick={()=>{props.history.push({pathname:"/musiclist", state:{...item, ...backColor[index]}})}}>
                            <div className='img'>
                                <img src={item.coverImgUrl} alt=""/>
                            </div>
                            <div className='miclist'>
                                {
                                    item.tracks.slice(0,3).map((subitem, subindex) => {
                                        return(
                                            <div 
                                            className='subitems'  key={subitem.name}>{subindex+1}.{subitem.name} - {getauthor(subitem.ar)}</div>
                                        )
                                    })
                                }
                            </div>
                        </div>
                    )
                })
            }
        </div>
    </div>
  )
}
