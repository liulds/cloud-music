import './app.scss';
import { Route, Switch } from 'react-router-dom'
import Index from './components/index/Index';
import Musiclist from './components/musiclist/Musiclist';
import Musicdetail from './components/musicdetail/Musicdetail';
import Musicdetail2 from './components/musicdetail/Musicdetail2';
import Searchmusic from './components/searchmusic/Searchmusic';
function App() {

  return (
    <div className="App">
      <Switch>
        <Route path='/' exact component={Index}/>
        <Route path='/musiclist' component={Musiclist}/>
        <Route path='/musicdetail' component={Musicdetail}/>
        <Route path='/musicdetail2' component={Musicdetail2}/>
        <Route path='/searchmusic' component={Searchmusic}/>
      </Switch>
    </div>
  );
}

export default App;
